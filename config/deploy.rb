# config valid only for Capistrano 3.1

require 'aws-sdk-v1'

set :keep_releases, 3
set :application, "api"
set :repo_url, "git@github.com:customerlobby/#{fetch(:application)}.git"
set :config_folder, '/home/ubuntu/sites/config/current'
set :service_name,  "#{fetch(:application)}_unicorn"

# Default branch is :develop
ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# Default deploy_to directory is /var/www/my_app
set :deploy_to, "/home/ubuntu/sites/#{fetch(:application)}"

# Default value for :scm is :git
set :scm, :git

set :unicorn_conf, "#{fetch(:deploy_to)}/current/config/unicorn.rb"
set :unicorn_pid,  "#{fetch(:deploy_to)}/shared/pids/unicorn.pid"

set :user, "ubuntu"
set :group, "ubuntu"
set :runner, fetch(:user)

set :resque_environment_task, true

$ec2 = AWS::EC2.new(:access_key_id => 'AKIAJRFGDWZSJA2KJYIA',:secret_access_key => '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')

# Link the log directory to ../shared/log
set :linked_dirs, %w{log}

namespace :resque do
  desc "Start Resque workers"
  task :start do
    fetch(:workers).each do |worker, queue_mapping|
      on roles(worker) do
        worker_id = 1
        queue_mapping.each do |queue, count|
          puts "Starting #{count} worker(s) with QUEUE: #{queue}"
          count.times do
            pid = "#{shared_path}/pids/resque_work_#{worker_id}.pid"

            within release_path do
              with rails_env: fetch(:rails_env) do
                execute :rake, "environment resque:work", "NEW_RELIC_DISPATCHER=resque", "PIDFILE=#{pid}", "QUEUE=#{queue}", ">> #{shared_path}/log/resque.log 2>&1 &"
              end
            end

           worker_id +=1
          end
        end
      end
    end
   end

   # VVERBOSE=1 RAILS_ENV=production bundle exec rake environment resque:work NEW_RELIC_DISPATCHER=resque QUEUES=foo,bus_incoming PIDFILE=/home/ubuntu/sites/api/shared/pids/resque_dispatcher_4.pid

   desc "Register Service"
   task :register_service do
     on roles(*fetch(:workers).keys) do
       within release_path do
         with rails_env: fetch(:rails_env) do
           execute :rake, "environment queuebus:subscribe"
         end
       end
     end
   end


  desc "Quit running Resque workers"
  task :stop do
     on roles(*fetch(:workers).keys) do
       if test "[ -e #{shared_path}/pids/resque_work_1.pid ]"
         within current_path do
           pids = capture(:ls, "-1 #{shared_path}/pids/resque_work*.pid")
           pids.each_line do |pid_file|
             begin
              execute :kill, "-s QUIT $(cat #{pid_file.chomp}) && rm #{pid_file.chomp}"
             rescue Exception => e

             end
           end
         end
       end
     end
   end

  desc "See current worker status"
  task :status do
    on roles(*fetch(:workers).keys) do
      if test "[ -e #{shared_path}/pids/resque_work_1.pid ]"
        within shared_path do
          files = capture(:ls, "-1 #{shared_path}/pids/resque_work*.pid")
          files.each_line do |file|
            info capture(:ps, "-f -p $(cat #{file.chomp}) | sed -n 2p")
          end
        end
      end
    end
  end

  desc "Restart running Resque workers"
  task :restart do
      invoke "resque:stop"
      invoke "resque:start"
    end
end

namespace :resque do
  namespace :scheduler do
    desc "Start the Resque Scheduler"
    task :start do
      on roles(:resque_worker) do
        within release_path do
          with rails_env: fetch(:rails_env) do
             pid_file = "#{shared_path}/pids/resque_scheduler.pid"
             execute :rake, "environment resque:scheduler", "NEW_RELIC_DISPATCHER=resque", "PIDFILE=#{pid_file}", ">> #{shared_path}/log/resque_scheduler.log 2>&1 &"
          end
        end
      end
    end

    desc "Stop the Resque Scheduler"
    task :stop do
      on roles(:resque_worker) do
        if test "[ -e #{shared_path}/pids/resque_scheduler.pid ]"
          within current_path do
            pids = capture(:ls, "-1 #{shared_path}/pids/resque_scheduler.pid")
            pids.each_line do |pid_file|
              execute :kill, "-s QUIT $(cat #{pid_file.chomp}) && rm #{pid_file.chomp}"
            end
          end
        end
      end
    end

    desc "Restart the Resque Scheduler"
    task :restart do
      invoke "resque:scheduler:stop"
      invoke "resque:scheduler:start"
    end

    desc "See Resque Scheduler Status"
    task :status do
      on roles(:resque_worker) do
        if test "[ -e #{shared_path}/pids/resque_scheduler.pid ]"
          within shared_path do
            files = capture(:ls, "-1 #{shared_path}/pids/resque_scheduler.pid")
            files.each_line do |file|
              info capture(:ps, "-f -p $(cat #{file.chomp}) | sed -n 2p")
            end
          end
        end
      end
    end
  end
end

namespace :deploy do

  desc 'Copy configuration example files to usable YAML files.'
  task :copy_settings do
    on roles(:app, :db, :worker01) do
      files = [
        'database','customerlobby','facebook','aws','googl','communication_credentials',
        'email','freshbooks','mongoid', 'obscenity', 'payments', 'pusher', 'quickbooks','rabbit','resque', 'schedule', 'shopify', 'sugarcrm',
        'twilio','twitter','cybersource', 'xero', 'direct_connect_ftp','rabbitmq', 'valid_email_suffix'
      ]

      files.each do |config_type|
        execute :ln, "-s #{fetch(:config_folder)}/#{config_type}.yml #{fetch(:release_path)}/config/#{config_type}.yml"
      end

      ['redis','newrelic'].each do |config_type|
        execute :ln, "-s #{fetch(:config_folder)}/#{config_type}/api.yml #{fetch(:release_path)}/config/#{config_type}.yml"
      end

    end
  end

  desc 'Restart the unicorn server'
  task :restart do
    on roles(:app) do
      execute :sudo, "service #{fetch(:service_name)} restart"
    end
  end

  desc 'Stop the unicorn server'
  task :start do
    on roles(:app) do
      execute :sudo, "service #{fetch(:service_name)} start"
    end
  end

  desc 'Start the unicorn server'
  task :stop do
    on roles(:app) do
      execute :sudo, "service #{fetch(:service_name)} stop"
    end
  end

  desc 'Check the status of unicorn server'
  task :status do
    on roles(:app) do
      execute :sudo, "status #{fetch(:service_name)}"
    end
  end

  desc 'Restart all the resque jobs and the scheduler'
  task :restart_jobs do
    invoke "resque:scheduler:restart" if fetch(:rails_env) == :production
    invoke "resque:register_service"
    invoke "resque:restart"
  end

  before :updated, :copy_settings

  after :publishing, :restart
  after :restart, :restart_jobs

end


namespace :ec2 do
  desc "Restart running Resque workers"
  task :get_instances do
    instances = get_instances()
    instances.each do |instance|
      puts [instance.id, instance.instance_type, instance.ip_address, instance.dns_name, instance.tags[:Environment], instance.tags[:Role]].join("\t")
    end
  end
end

def get_instances()
  return $ec2.instances.select {|instance| instance.tags.to_h["Environment"] == "#{fetch(:rails_env).to_s}"}
end

def filter_instances_by_role(role)
  get_instances.select {|instance| instance.tags.to_h['Role'] == role}
end
