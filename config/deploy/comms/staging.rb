set :rails_env, :staging
set :host, ["#{fetch(:user)}@ec2-174-129-67-105.compute-1.amazonaws.com"] # We need to be able to SSH to that box as this user.
role :resque_worker, fetch(:host)

set :ssh_options, {
  keys: [File.join("~", ".ssh", "customerlobby.pem")]
}

set :workers, { "communications,customer_communication_service_default" => 1}

