set :rails_env, :staging
set :host, filter_instances_by_role("app").map {|instance| "#{fetch(:user)}@#{instance.dns_name}"}   
role :web, fetch(:host)
role :app, fetch(:host)
role :db,  fetch(:host), :primary => true
role :worker00, fetch(:host)

set :ssh_options, {
  keys: [File.join("~", ".ssh", "customerlobby.pem")]
}

set :workers, { 
  worker00:{
    "direct_connect_verifier,medium,contact,light,mailer,crm,analytics,updates,tracking,payments,appointments,data_processor_service_default,scotty" => 1
  }  
}